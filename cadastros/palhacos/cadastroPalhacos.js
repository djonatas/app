'use strict';

angular.module('myApp.cadastroPalhacos', ['ngRoute', 'ui.mask'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/palhacos', {
            templateUrl: 'cadastros/palhacos/cadastroPalhacos.html',
            controller: 'ViewCadastroPalhacosController'
        });

        $routeProvider.when('/palhacos/:id', {
            templateUrl: 'cadastros/palhacos/cadastroPalhacos.html',
            controller: 'ViewCadastroPalhacosController'
        });
    }])

    .controller('ViewCadastroPalhacosController', ['$scope', '$http', '$routeParams', '$location',
        function($scope, $http, $routeParams, $location){

        $scope.listaEntidades = [];
        $scope.listaCelulas = [];

        $scope.data = {
            humano: '',
            palhaco: '',
            cpf: '',
            rg: '',
            celulaId: '',
            endereco: '',
            complemento: '',
            bairro: '',
            CEP: '',
            cidade: '',
            estado: '',
            telefone: '',
            celular: '',
            profissao: '',
            empresa: '',
            email: '',
            central: 0,
            dataAniversario: null,
            dataEntradaONG: null,
            dataCadastro: new Date(),
            funcao: 0,
        };

        if ($routeParams.id){
            buscarPalhacoById(parseInt($routeParams.id));
            $scope.mode = 4;
        } else {
            $scope.mode = 1;
        }

        $scope.salvarPalhacoRequest = function (){
            if ($scope.mode === 4){
                alterarRegistro();
            } else {
                salvarNovo();
            }
        };

        $scope.buscarPalhaco = function(terrmoBusca){
            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/clown?search='+terrmoBusca,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    if (data.data.success){
                        $scope.retornoBusca = data.data.data;
                        $scope.mode = 3;
                    }

                    console.log(data);
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        };

        $scope.selecionarEntidade = function(){
            if ($scope.data.central){
                buscarEntidadeByCentral($scope.data.central);
            }
        };

        $scope.selecionarCelulas = function(){
            if ($scope.data.entidadeId){
                buscarCelulaByEntidade($scope.data.entidadeId);
            }
        };

        function buscarPalhacoById(palhacoId){

            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/clown?id='+palhacoId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    if (data.data.success &&  data.data.data[0]){
                        $scope.data = data.data.data[0];
                        $scope.data.central = data.data.data[0].central.toString();
                        $scope.data.dataCadastro = new Date(data.data.data[0].dataCadastro);

                        if (Date.dataValida(data.data.data[0].dataEntradaONG)){
                            $scope.data.dataEntradaONG = new Date(data.data.data[0].dataEntradaONG);
                        }

                        if (Date.dataValida(data.data.data[0].dataAniversario)){
                            $scope.data.dataAniversario = new Date(data.data.data[0].dataAniversario);
                        }


                        resolverCombos(data.data.data[0]);
                    } else {
                        alert('não encontrado');
                        $location.path('/palhacos');
                    }
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function salvarNovo(){
            var req = {
                method: 'POST',
                url: 'https://presentealegriaapi.herokuapp.com/clown',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){

                    if (data.data.success === true){
                        alert('Palhaço registrada com sucesso');
                        $location.path('/palhacos');
                        $scope.mode = 1;
                        $scope.termoBusca = '';
                        console.log(data);
                    } else {
                        alert('erro');
                        console.log(data.data);
                    }

                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function alterarRegistro(){
            var req = {
                method: 'PUT',
                url: 'https://presentealegriaapi.herokuapp.com/clown',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    alert('Palhaço alterada com sucesso');
                    $location.path('/palhacos');
                }, function(error){
                    alert('Falha ao alterar registro');
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function buscarEntidadeByCentral(centralId, resolving){

            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/entity?central='+centralId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    if (data.data.success &&  data.data.data[0]){

                        $scope.listaEntidades = data.data.data;

                        if (resolving){
                            $scope.data.entidadeId = $scope.data.entidadeId.toString();
                            buscarCelulaByEntidade($scope.data.entidadeId, true);
                        }
                    } else {
                        if (resolving) return;
                        alert('Nenhuma instituição encontrada para esta central');
                    }
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function buscarCelulaByEntidade(entidadeId, resolving){
            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/cell?entityId='+entidadeId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    if (data.data.success &&  data.data.data[0]){
                        $scope.listaCelulas = data.data.data;

                        if (resolving){
                            $scope.data.celulaId = $scope.data.celulaId.toString();
                        }

                    } else {
                        if (resolved) return;
                        alert('Nenhuma celula encontrada para esta instituição');
                    }
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function resolverCombos(data){
            buscarEntidadeByCentral(data.central, resolveComboCelula);
        }

        function resolveComboCelula(){

        }

    }]);