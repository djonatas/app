'use strict';

angular.module('myApp.cadastroPadrinhos', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/padrinhos', {
            templateUrl: 'cadastros/padrinhos/cadastroPadrinhos.html',
            controller: 'ViewCadastroPadrinhosController'
        });

        $routeProvider.when('/padrinhos/:id', {
            templateUrl: 'cadastros/padrinhos/cadastroPadrinhos.html',
            controller: 'ViewCadastroPadrinhosController'
        });
    }])

    .controller('ViewCadastroPadrinhosController', ['$scope', '$http', '$routeParams', '$location',
        function($scope, $http, $routeParams, $location){


        $scope.data = {
            humano: '',
            palhaco: '',
            cpf: '',
            rg: '',
            endereco: '',
            complemento: '',
            bairro: '',
            CEP: '',
            cidade: '',
            estado: '',
            telResidencial: '',
            celular: '',
            profissao: '',
            empresa: '',
            email: '',
            central: 0,
            celulaIdHospital: null,
            dtCelulaHospital: null,
            celulaIdAsilo: null,
            dtCelulaAsilo: null,
            celulaIdAbrigo: null,
            dtCelulaAbrigo: null,
            dataAniversario: new Date(),
            dataEntradaONG: new Date(),
            dataCadastro: new Date(),
            padrinho: 1,
            formado: 0,
            entrevista: '',
            skype: '',
            instagram: '',
            facebook: '',
            feedbackHospital: '',
            feedbackAbrigo: '',
            feedbackAsilo: ''
        };

        if ($routeParams.id){
            buscarPadrinhoById(parseInt($routeParams.id));
            $scope.mode = 4;
        } else {
            $scope.mode = 1;
        }

        $scope.salvarPadrinhoRequest = function (){
            if ($scope.mode === 4){
                alterarRegistro();
            } else {
                salvarNovo();
            }
        };

        $scope.buscarPadrinho = function(terrmoBusca){
            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/godfather?search='+terrmoBusca,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    if (data.data.success){
                        $scope.retornoBusca = data.data.data;
                        $scope.mode = 3;
                    }

                    console.log(data);
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        };

        $scope.retornarCelulas = function(){
            if ($scope.data && $scope.data.central){
                retornarCelulasAsilo($scope.data.central, false);
                retornarCelulasAbrigo($scope.data.central, false);
                retornarCelulasHospital($scope.data.central, false);
            }
        };

        function buscarPadrinhoById(institutoId){

            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/godfather?id='+institutoId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    if (data.data.success &&  data.data.data[0]){
                        $scope.data = data.data.data[0];
                        $scope.data.central = data.data.data[0].central.toString();

                        $scope.data.dataCadastro = new Date(data.data.data[0].dataCadastro);

                        if (data.data.data[0].dtCelulaHospital){
                            $scope.data.dtCelulaHospital = new Date(data.data.data[0].dtCelulaHospital);
                        }

                        if (data.data.data[0].dtCelulaAsilo){
                            $scope.data.dtCelulaAsilo = new Date(data.data.data[0].dtCelulaAsilo);
                        }

                        if (data.data.data[0].dtCelulaAbrigo){
                            $scope.data.dtCelulaAbrigo = new Date(data.data.data[0].dtCelulaAbrigo);
                        }

                        if (data.data.data[0].dataAniversario){
                            $scope.data.dataAniversario = new Date(data.data.data[0].dataAniversario);
                        }

                        if (data.data.data[0].dataEntradaONG){
                            $scope.data.dataEntradaONG = new Date(data.data.data[0].dataEntradaONG);
                        }

                        if (data.data.data[0].dataCadastro){
                            $scope.data.dataCadastro = new Date(data.data.data[0].dataCadastro);
                        }

                        resolveCombosPadrinhos();

                    } else {
                        alert('não encontrado');
                        $location.path('/padrinhos');
                        $scope.mode = 1;
                    }
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function salvarNovo(){
            var req = {
                method: 'POST',
                url: 'https://presentealegriaapi.herokuapp.com/godfather',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){

                    if (data.data.success === true){
                        alert('Padrinho salvo com sucesso');
                        $location.path('/padrinhos');
                        console.log(data);
                    } else {
                        alert('erro');
                        console.log(data.data);
                    }

                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function alterarRegistro(){
            var req = {
                method: 'PUT',
                url: 'https://presentealegriaapi.herokuapp.com/godfather',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    alert('Padrinho alterada com sucesso');
                    $location.path('/padrinhos');
                }, function(error){
                    alert('Falha ao alterar registro');
                    console.log(error);
                    console.log('ERROR');
                });
        }


        function retornarCelulasAsilo(centralId, resolving){
            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/cell?segmentId=1&centralId='+centralId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){


                    if (data.data.success && data.data.success === true){
                        $scope.listaAsilo = [];
                        data.data.data.forEach((item) => {
                            $scope.listaAsilo.push(item);
                        });

                        if (resolving){

                            $scope.data.celulaIdAsilo = $scope.data.celulaIdAsilo.toString();
                        }
                    }
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function retornarCelulasAbrigo(centralId, resolving){
            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/cell?segmentId=2&centralId='+centralId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    $scope.listaAbrigo = [];
                    $scope.listaAbrigo.push( { id: 0, nome: 'Nenhum', entidadeNome: 'Nenhum' } );

                    if (data.data.success && data.data.success === true){
                        $scope.listaAbrigo = data.data.data;

                        if (resolving){
                            $scope.data.celulaIdAbrigo = $scope.data.celulaIdAbrigo.toString();
                        }
                    }
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function retornarCelulasHospital(centralId, resolving){
            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/cell?segmentId=3&centralId='+centralId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    $scope.listaHospital = [];
                    $scope.listaHospital.push( { id: 0, nome: 'Nenhum', entidadeNome: 'Nenhum' } );

                    if (data.data.success && data.data.success === true){
                        $scope.listaHospital = data.data.data;

                        if (resolving){
                            $scope.data.celulaIdHospital = $scope.data.celulaIdHospital.toString();
                        }
                    }
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function resolveCombosPadrinhos(){
            retornarCelulasAsilo($scope.data.central, true);
            retornarCelulasAbrigo($scope.data.central, true);
            retornarCelulasHospital($scope.data.central, true);
        }

    }]);