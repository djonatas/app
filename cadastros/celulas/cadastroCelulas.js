'use strict';

angular.module('myApp.cadastroCelulas', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/celulas', {
            templateUrl: 'cadastros/celulas/cadastroCelulas.html',
            controller: 'ViewCadastroCelulasController'
        });

        $routeProvider.when('/celulas/:id', {
            templateUrl: 'cadastros/celulas/cadastroCelulas.html',
            controller: 'ViewCadastroCelulasController'
        });
    }])

    .controller('ViewCadastroCelulasController', ['$scope', '$http', '$routeParams', '$location',
        function($scope, $http, $routeParams, $location){

            $scope.data = {
                nome: '',
                dataCriacao: null,
                coordenadorId: 0,
                entidadeId: 0,
                central: 0,
                diaVisita: null
            };

            if ($routeParams.id){
                buscarCelululaById(parseInt($routeParams.id));
                $scope.mode = 4;
            } else {
                $scope.mode = 1;
            }

            $scope.salvarCelulaRequest = function (){

                if ($scope.data.central === 0){
                    alert('Selecione a central da célula');
                    return;
                }
                if ($scope.data.entidadeId === 0){
                    alert('Selecione uma entidade');
                    return;
                }

                if ($scope.mode === 4){
                    alterarRegistro();
                } else {
                    salvarNovo();
                }
            };

            $scope.buscarCelula = function(terrmoBusca){
                var req = {
                    method: 'GET',
                    url: 'https://presentealegriaapi.herokuapp.com/cell?search='+terrmoBusca,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.data
                };

                $http(req)
                    .then(function(data){
                        if (data.data.success){
                            $scope.retornoBusca = data.data.data;
                            $scope.mode = 3;
                        }

                        console.log(data);
                    }, function(error){
                        console.log(error);
                        console.log('ERROR');
                    });
            };

            $scope.selecionarInstituicoes = function(){
                if ($scope.data.central){
                    buscarInstituicaoByCentral($scope.data.central);
                }
            };

            function buscarCelululaById(celulaId){

                var req = {
                    method: 'GET',
                    url: 'https://presentealegriaapi.herokuapp.com/cell?id='+celulaId,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.data
                };

                $http(req)
                    .then((data) => {
                        if (data.data.success &&  data.data.data[0]){
                            buscarInstituicaoByCentral(data.data.data[0].central, data.data.data[0]);
                        } else {
                            alert('não encontrado');
                            $location.path('/celulas');
                        }
                    })
                    .then(function(data){
                        if (data.data.success &&  data.data.data[0]){
                            $scope.data = data.data.data[0];
                            $scope.data.central = data.data.data[0].central.toString();
                            $scope.data.diaVisita = data.data.data[0].diaVisita.toString();
                        } else {
                            alert('não encontrado');
                            $location.path('/celulas');
                        }
                    }, function(error){
                        console.log(error);
                        console.log('ERROR');
                    });
            }

            function salvarNovo(){
                var req = {
                    method: 'POST',
                    url: 'https://presentealegriaapi.herokuapp.com/cell',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.data
                };

                $http(req)
                    .then(function(data){

                        if (data.data.success === true){
                            alert('Celula registrada com sucesso');
                            $location.path('/celulas');
                            $scope.mode = 1;
                            $scope.termoBusca = '';
                            console.log(data);
                        } else {
                            alert('erro');
                            console.log(data.data);
                        }

                    }, function(error){
                        console.log(error);
                        console.log('ERROR');
                    });
            }

            function alterarRegistro(){
                var req = {
                    method: 'PUT',
                    url: 'https://presentealegriaapi.herokuapp.com/cell',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.data
                };

                $http(req)
                    .then(function(data){
                        alert('Celula alterada com sucesso');
                        $location.path('/celulas');
                    }, function(error){
                        alert('Falha ao alterar registro');
                        console.log(error);
                        console.log('ERROR');
                    });
            }

            function buscarInstituicaoByCentral(centralId, dataFull){
                var req = {
                    method: 'GET',
                    url: 'https://presentealegriaapi.herokuapp.com/entity?central='+centralId,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.data
                };

                $http(req)
                    .then(function(data){
                        if (data.data.success &&  data.data.data[0]){

                            $scope.listaInstituicoes = data.data.data;

                            if (dataFull){
                                $scope.data = dataFull;
                                $scope.data.central = dataFull.central.toString();
                                $scope.data.diaVisita = dataFull.diaVisita.toString();
                                $scope.data.entidadeId = dataFull.entidadeId.toString();
                            }

                        } else {

                            if (dataFull){
                                $scope.data = dataFull;
                                $scope.data.central = dataFull.central.toString();
                                $scope.data.diaVisita = dataFull.diaVisita.toString();
                                $scope.data.entidadeId = dataFull.entidadeId.toString();
                            } else {
                                alert('Nenhuma instituição encontrada para esta central');
                                $scope.data.entidadeId = 0;
                                $scope.data.central = 0;
                            }
                        }
                    }, function(error){
                        console.log(error);
                        console.log('ERROR');
                    });
            }

    }]);