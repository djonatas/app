'use strict';

angular.module('myApp.cadastroInstituicao', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/instituicao', {
            templateUrl: 'cadastros/instituicao/cadastroInstituicao.html',
            controller: 'ViewCadastroInstituicaoController'
        });

        $routeProvider.when('/instituicao/:id', {
            templateUrl: 'cadastros/instituicao/cadastroInstituicao.html',
            controller: 'ViewCadastroInstituicaoController'
        });
    }])

    .controller('ViewCadastroInstituicaoController', ['$scope', '$http', '$routeParams', '$location',
        function($scope, $http, $routeParams, $location){

        $scope.data = {
            nome: '',
            contato: '',
            telefone: '',
            email: '',
            endereco: '',
            complemento: '',
            bairro: '',
            cep: '',
            cidade: '',
            estado: '',
            central: 0,
            segmento: 0
        };

        if ($routeParams.id){
            buscarInstitutoById(parseInt($routeParams.id));
            $scope.mode = 4;
        } else {
            $scope.mode = 1;
        }

        $scope.salvarInstitutoRequest = function (){
            if ($scope.mode === 4){
                alterarRegistro();
            } else {
                salvarNovo();
            }
        };

        $scope.buscarInstituicao = function(terrmoBusca){
            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/entity?search='+terrmoBusca,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    if (data.data.success){
                        $scope.retornoBusca = data.data.data;
                        $scope.mode = 3;
                    }

                    console.log(data);
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        };

        function buscarInstitutoById(institutoId){

            var req = {
                method: 'GET',
                url: 'https://presentealegriaapi.herokuapp.com/entity?id='+institutoId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    if (data.data.success &&  data.data.data[0]){
                        $scope.data = data.data.data[0];
                        $scope.data.central = data.data.data[0].central.toString();

                        if (data.data.data[0].segmento){
                            $scope.data.segmento = data.data.data[0].segmento.toString();
                        }

                    } else {
                        alert('não encontrado');
                        $location.path('/instituicao');
                        $scope.mode = 1;
                    }
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function salvarNovo(){
            var req = {
                method: 'POST',
                url: 'https://presentealegriaapi.herokuapp.com/entity',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){

                    if (data.data.success === true){
                        alert('Instituição registrada com sucesso');
                        $location.path('/instituicao');
                        console.log(data);
                    } else {
                        alert('erro');
                        console.log(data.data);
                    }

                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        }

        function alterarRegistro(){
            var req = {
                method: 'PUT',
                url: 'https://presentealegriaapi.herokuapp.com/entity',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    alert('Intituição alterada com sucesso');
                    $location.path('/instituicao');
                }, function(error){
                    alert('Falha ao alterar registro');
                    console.log(error);
                    console.log('ERROR');
                });
        }
    }]);